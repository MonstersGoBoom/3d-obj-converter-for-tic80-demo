
obj_loader = require("obj_loader")

--	convert to interger values to save space 

local XSCALE = 256.0
local YSCALE = 256.0
local ZSCALE = 256.0
local USCALE = 64.0
local VSCALE = 64.0
local NSCALE = 256.0

function OBJ2TIC(filename)
	local object = obj_loader.load(filename)

	objectname = "Mesh_" .. string.gsub(filename, ".obj", "")
	print( objectname )

	file = io.open (objectname .. ".lua","w")
	io.output(file)

	io.write(objectname .. " = {\n")

	local vrts = ""
	for x=1,#object.v do
		v = object.v[x]
		vrts = vrts .. string.format( "%d,%d,%d,",(v.x*XSCALE)//1,(v.y*YSCALE)//1,(v.z*ZSCALE)//1)	
	end
	io.write("\tv = { " .. vrts .. " },\n")
	--print("v = { " .. vrts .. " },")
	
	local tvrts = ""
	for x=1,#object.vt do
		vt = object.vt[x]
		tvrts = tvrts .. string.format( "%d,%d,",(vt.u*USCALE)//1,(vt.v*VSCALE)//1)	
	end
	io.write("\tvt = { " .. tvrts .. " },\n")

	local nvrts = ""
	for x=1,#object.vn do
		vn = object.vn[x]
		nvrts = nvrts .. string.format( "%d,%d,%d",(vn.x*NSCALE)//1,(vn.y*NSCALE)//1,(vn.z*NSCALE)//1)	
	end
	io.write("\tvn = { " .. nvrts .. " },\n")
--

--	print("vt = { " .. tvrts .. " },")
	
	local tris = ""
	local textris = ""
	local quads = ""
	local texquads = ""
	for x=1,#object.f do
		f = object.f[x]
		if #f==3 then 
			print(f[1].vt)
			if f[1].vt == nil or f[2].vt == nil or f[3].vt == nil then 
				tris = tris .. string.format("%d,%d,%d,",f[1].v,f[2].v,f[3].v)
				tris = tris .. string.format("%d,%d,%d,",f[1].vn,f[2].vn,f[3].vn)
			else
				textris = textris .. string.format("%d,%d,%d,",f[1].v,f[2].v,f[3].v)
				textris = textris .. string.format("%d,%d,%d,",f[1].vt,f[2].vt,f[3].vt)
			end
		elseif #f==4 then 
			if f[1].vt == nil or f[2].vt == nil or f[3].vt == nil or f[4].vt == nil then 
				quads = quads .. string.format("%d,%d,%d,%d,",f[1].v,f[2].v,f[3].v,f[4].v)
				quads = quads .. string.format("%d,%d,%d,%d,",f[1].vn,f[2].vn,f[3].vn,f[4].vn)
			else
				texquads = texquads .. string.format("%d,%d,%d,%d,",f[1].v,f[2].v,f[3].v,f[4].v)
				texquads = texquads .. string.format("%d,%d,%d,%d,",f[1].vt,f[2].vt,f[3].vt,f[4].vt)
			end
		end
--[[
		if #f==3 then 
			tris = tris .. string.format("%d,%d,%d,",f[1].v,f[2].v,f[3].v)
			if f[1].vt == nil or f[2].vt == nil or f[3].vt == nil then 
				tris = tris .. string.format("1,1,1,")
			else
				tris = tris .. string.format("%d,%d,%d,",f[1].vt,f[2].vt,f[3].vt)
			end
		elseif #f==4 then 
			quads = quads .. string.format("%d,%d,%d,%d,",f[1].v,f[2].v,f[3].v,f[4].v)
			if f[1].vt == nil or f[2].vt == nil or f[3].vt == nil or f[4].vt == nil then 
				quads = quads .. string.format("1,1,1,1,")
			else
				quads = quads .. string.format("%d,%d,%d,%d,",f[1].vt,f[2].vt,f[3].vt,f[4].vt)
			end
		else
			print(#f)
		end
]]--		
	end
	io.write("\tt = { " .. tris .. " },\n")
	io.write("\ttt = { " .. textris .. " },\n")
--	print("t = {" .. tris .. "},")
	io.write("\tq = { " .. quads .. " },\n")
	io.write("\ttq = { " .. texquads .. " },\n")
--	print("q = {" .. quads .. "},")
	io.write("}\n")
	io.close(file)
	
end

local args = {...}
for i, v in ipairs(args) do
	if string.match(v, ".obj") then 
		OBJ2TIC(v)
	end
end 





